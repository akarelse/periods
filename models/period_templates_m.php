<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Periods Module
 */
class Period_templates_m extends MY_Model
{
    
    /**
     * Constructor
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->_table = 'period_templates';
    }
    
    /**
     * create an period template
     * @param type $input
     * @return boolean
     */
    public function create($input) {
        $to_insert = array(
            'name' => $input['name'],
            'slug' => $this->_check_slug($input['slug']) ,
            'temphigh' => $input['temphigh'],
            'templow' => $input['templow'],
            'phhigh' => $input['phhigh'],
            'phlow' => $input['phlow'],
            'echigh' => $input['echigh'],
            'eclow' => $input['eclow'],
            'waterhigh' => $input['waterhigh'],
            'waterlow' => $input['waterlow'],
            'moisthigh' => $input['moisthigh'],
            'moistlow' => $input['moistlow'],
            'co2high' => $input['co2high'],
            'co2low' => $input['co2low']
        );
        
        return $this->db->insert('period_templates', $to_insert);
    }
    
    /**
     * make sure the slug is valid
     * @param string $slug
     * @return string
     */
    public function _check_slug($slug) {
        $slug = strtolower($slug);
        $slug = preg_replace('/\s+/', '-', $slug);
        return $slug;
    }
    
    /**
     * To edit a period template
     * @param type $id
     * @param type $input
     * @return boolean
     */
    public function edit($id, $input) {
        $this->db->trans_start();
        
        // validate the data and update
        $result = $this->update($id, array(
            'name' => $input['name'],
            'slug' => $this->_check_slug($input['slug']) ,
            'temphigh' => $input['temphigh'],
            'templow' => $input['templow'],
            'phhigh' => $input['phhigh'],
            'phlow' => $input['phlow'],
            'echigh' => $input['echigh'],
            'eclow' => $input['eclow'],
            'waterhigh' => $input['waterhigh'],
            'waterlow' => $input['waterlow'],
            'moisthigh' => $input['moisthigh'],
            'moistlow' => $input['moistlow'],
            'co2high' => $input['co2high'],
            'co2low' => $input['co2low']
        ));
        
        // did it pass validation?
        if (!$result) return false;
        $input['id'] = $id;
        $this->db->trans_complete();
        return (bool)$this->db->trans_status();
    }
}
