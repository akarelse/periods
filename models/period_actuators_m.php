<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Periods Module
 */
class Period_actuators_m extends MY_Model
{
    
    /**
     * Constructor
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->_table = 'period_actuators';
    }
    
    /**
     * create an period template
     * @param type $input
     * @return boolean
     */
    public function create($input) {
        $to_insert = array(
            'name' => $input['name'],
            'slug' => $this->_check_slug($input['slug']) ,
            'type_id' => $input['type_id']
        );
        
        return $this->db->insert($this->_table, $to_insert);
    }
    
    /**
     * make sure the slug is valid
     * @param string $slug
     * @return string
     */
    public function _check_slug($slug) {
        $slug = strtolower($slug);
        $slug = preg_replace('/\s+/', '-', $slug);
        return $slug;
    }
    
    /**
     * To edit a period template
     * @param type $id
     * @param type $input
     * @return boolean
     */
    public function edit($id, $input) {
        $this->db->trans_start();
        
        $result = $this->update($id, array(
            'name' => $input['name'],
            'slug' => $this->_check_slug($input['slug']) ,
            'type_id' => $input['type_id']
        ));
        
        if (!$result) return false;
        $input['id'] = $id;
        $this->db->trans_complete();
        return (bool)$this->db->trans_status();
    }
}
