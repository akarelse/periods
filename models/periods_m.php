<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Periods Module
 */
class Periods_m extends MY_Model
{
    
    /**
     * Constructor
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->_table = 'periods';
    }
    
    /**
     * get current period template
     * @return the current template results
     */
    public function get_current() {
        $date = new DateTime();
        $this->db->select($this->_table . ".id, period_templates.*");
        $this->db->from($this->_table);
        $this->db->where("starttime < '" . $date->format('Y-m-d H:i:s') . "' AND " . "stoptime > '" . $date->format('Y-m-d H:i:s') . "'");
        $this->db->join("period_templates", "period_templates.id = " . $this->_table . ".template_id", "RIGHT");
        
        $results = $this->db->get()->result();
        if (count($results) > 0) {
            $results = $results[0];
        }
        
        unset($results->id);
        unset($results->slug);
        return $results;
    }
    
    /**
     * get all peroids with their templates
     * @return database result array
     */
    public function get_all() {
        $this->db->select($this->_table . '.*, period_templates.name as tname');
        $this->db->from($this->_table);
        $this->db->join('period_templates', 'period_templates.id = ' . $this->_table . '.template_id', 'LEFT');
        $results = $this->db->get()->result();
        return $results;
    }
    
    public function last_added() {
        return $this->db->order_by("id", "desc")->get($this->_table, 1, 0)->result();
    }
    
    public function before($id) {
        return $this->db->where('id', $id - 1)->get($this->_table, 1, 0)->result();
    }
    
    /**
     * a create function for controller periods
     * @param array post input stuff
     * @return boolean
     */
    public function create($input) {
        
        $to_insert = array(
            'name' => $input['name'],
            'slug' => $this->_check_slug($input['slug']) ,
            'starttime' => $input['starttime'],
            'duration' => $input['duration'],
            'template_id' => $input['template_id']
        );
        
        return $this->db->insert('periods', $to_insert);
    }
    
    public function forindex() {
        return $this->db->order_by("id", "desc")->get($this->_table)->result();
    }
    
    /**
     * make sure the slug is not sporting anny chars that do no belong in html/url
     * @param string $slug
     * @return string
     */
    public function _check_slug($slug) {
        $slug = strtolower($slug);
        $slug = preg_replace('/\s+/', '-', $slug);
        
        return $slug;
    }
    
    public function edit_plus_after($id, $input) {
        $this->db->trans_start();
        if ($this->edit($id, $input)) {
            $results = $this->db->where('id >=', $id)->get($this->_table)->result();
            for ($i = 0; $i < count($results); $i++) {
                if (isset($results[$i - 1])) {
                    $date = DateTime::createFromFormat('Y-m-d H:i:s', $results[$i - 1]->starttime);
                    $date->add(new DateInterval('P' . $results[$i - 1]->duration . 'D'));
                    $results[$i]->starttime = $date->format('Y-m-d H:i:s');
                }
                $this->firephp->log($results[$i]);
                $this->edit($results[$i]->id, $results[$i]);
            }
        } else {
            return false;
        }
        
        $this->db->trans_complete();
        return (bool)$this->db->trans_status();
    }
    
    /**
     * the edit function to edit items
     * @param type $id
     * @param type $input
     * @return boolean
     */
    public function edit($id, $input) {
        $this->db->trans_start();
        $input = (array)$input;
        $result = $this->update($id, array(
            'name' => $input['name'],
            'slug' => $input['slug'],
            'starttime' => $input['starttime'],
            'duration' => $input['duration'],
            'template_id' => $input['template_id']
        ));
        
        if (!$result) {
            return false;
        }
        
        $this->db->trans_complete();
        return (bool)$this->db->trans_status();
    }
}
