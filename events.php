<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Periods Events Class
 *
 * @package        PyroCMS
 * @subpackage    Periods Module
 * @category
 * @author
 * @website
 */
class Events_Periods
{
    
    protected $ci;
    
    public function __construct() {
        $this->ci = & get_instance();
        Events::register('current_period', array(
            $this,
            'get_current'
        ));
        Events::register('all_actuators', array(
            $this,
            'get_all_actuators'
        ));
    }
    
    public function get_current() {
        $this->ci->load->model('periods/periods_m');
        return $this->ci->periods_m->get_current();
    }
    
    public function get_all_actuators() {
        $this->ci->load->model('periods/period_actuators_m');
        return $this->ci->period_actuators_m->get_all();
    }
}