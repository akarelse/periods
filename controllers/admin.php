<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Periods Module
 */
class Admin extends Admin_Controller
{
    protected $section = 'items';
    
    /**
     * Constructor
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('periods_m');
        $this->load->model('period_templates_m');
        $this->load->library('form_validation');
        $this->load->library('firephp');
        
        $this->lang->load('periods');
        $this->item_validation_rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|max_length[100]|required'
            ) ,
            array(
                'field' => 'slug',
                'label' => 'Slug',
                'rules' => 'trim|max_length[100]|required'
            ) ,
            array(
                'field' => 'start_on',
                'label' => 'Start on',
                'rules' => 'trim|max_length[100]|required'
            ) ,
            array(
                'field' => 'start_on_minute',
                'label' => 'Start on minute',
                'rules' => 'trim|max_length[100]|required'
            ) ,
            array(
                'field' => 'start_on_hour',
                'label' => 'Start on hour',
                'rules' => 'trim|max_length[100]|required'
            ) ,
            array(
                'field' => 'duration',
                'label' => 'Duration',
                'rules' => 'trim|max_length[10]|required'
            ) ,
            array(
                'field' => 'template_id',
                'label' => 'Categorie',
                'rules' => 'trim|integer'
            )
        );
        
        $this->template->append_js('module::admin.js')->append_css('module::admin.css')->append_css('module::jquery-ui-1.10.4.custom.min.css')->append_metadata($this->load->view('fragments/wysiwyg'));
    }
    public function index() {
        $items = $this->periods_m->forindex();
        $this->template->title($this->module_details['name'])->set('items', $items)->build('admin/items');
    }
    
    /**
     * the admin controller for periods create funtion
     * @return void
     */
    public function create() {
        if ($input = $this->input->post()) {
            $data = $this->setrightpost($input);
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $input['start_on'] . " " . $input['start_on_hour'] . ":" . $input['start_on_minute'] . ":00");
            $data['starttime'] = $date->format('Y-m-d H:i:s');
            $this->form_validation->set_rules($this->item_validation_rules);
            
            if ($this->form_validation->run()) {
                if ($this->periods_m->create($data)) {
                    $this->session->set_flashdata('success', lang('periods.success'));
                    redirect('admin/periods');
                } else {
                    $this->session->set_flashdata('error', lang('periods.error'));
                    redirect('admin/periods/create');
                }
            }
        } else {
            foreach ($this->item_validation_rules as $rule) {
                $data[$rule['field']] = $input[$rule['field']];
            }
        }
        $temp = $this->periods_m->last_added();
        if (count($temp) > 0) {
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $temp[0]->starttime);
            $editabledate = false;
            $date->add(new DateInterval('P' . $temp[0]->duration . 'D'));
        } else {
            $editabledate = true;
            $date = new DateTime();
        }
        $data['editabledate'] = $editabledate;
        $data['templates'] = $this->period_templates_m->get_all();
        $data['hours'] = range(1, 24);
        $data['minutes'] = range(0, 60);
        $templates = array(
            0 => 'default'
        );
        foreach ($this->period_templates_m->get_all() as $template) {
            $templates[$template->id] = $template->name;
        }
        $data['templates'] = $templates;
        $this->template->title($this->module_details['name'], lang('periods.new_item'))->set('rightstarttime', $date->format('U'))
        ->build('admin/form', $data);
    }
    
    /**
     * the admin controller for periods edit funtion
     * @return void
     */
    public function edit($id = 0) {
        $data = $this->periods_m->get($id);
        if ($input = $this->input->post()) {
            $this->form_validation->set_rules($this->item_validation_rules);
            unset($_POST['btnAction']);
            $starttime = DateTime::createFromFormat('Y-m-d H:i:s', $this->input->post('start_on') . " " . $this->input->post('start_on_hour') . ":" . $this->input->post('start_on_minute') . ":00");
            $data = $this->setrightpost($this->input->post());
            $data['starttime'] = $starttime->format('Y-m-d H:i:s');
            if ($this->periods_m->edit_plus_after($id, $data) && $this->form_validation->run()) {
                $this->session->set_flashdata('success', "OK !");
                redirect('admin/periods');
            } else {
                $this->session->set_flashdata('error', lang('periods.error'));
            }
        } else {
            $starttime = DateTime::createFromFormat('Y-m-d H:i:s', $data->starttime);
        }
        $temp = $this->periods_m->before($id);
        if (count($temp) > 0) {
            $editabledate = false;
        } else {
            $editabledate = true;
        }
        $data->hours = range(1, 24);
        $data->minutes = range(0, 60);
        $templates = array(
            0 => 'default'
        );
        foreach ($this->period_templates_m->get_all() as $template) {
            $templates[$template->id] = $template->name;
        }
        $data->templates = $templates;
        $this->pyrocache->delete('periods_m');
        $this->template->title($this->module_details['name'], lang('periods.edit'))->set('rightstarttime', $starttime->format('U'))->build('admin/form', $data);
    }
    
    /**
     * remove date stuff after it is integrated into something else
     * @param  array $post post data in
     * @return array $post cleaned data out
     */
    private function setrightpost($post) {
        unset($post['start_on']);
        unset($post['start_on_hour']);
        unset($post['start_on_minute']);
        unset($post['btnAction']);
        return $post;
    }
    
    /**
     * a callback funtion for form validation
     *
     * @access public
     * @return boolean start date should be before stopdate
     */
    public function compare_dates() {
        $startdate = DateTime::createFromFormat('Y-m-d H:i:s', $this->input->post('start_on') . $this->input->post('start_on_hour') . ":" . $this->input->post('start_on_minute') . ":00");        
        $items = $this->periods_m->get_all();
        if (count($items) > 0) {
            foreach ($items as $item) {
                
                $dbstartdate = DateTime::createFromFormat('Y-m-d H:i:s', $item->starttime);
                $dbstopdate = DateTime::createFromFormat('Y-m-d H:i:s', $item->stoptime);
                
                if ($dbstartdate < $startdate && $dbstopdate > $stopdate || $dbstartdate > $startdate && $dbstopdate < $stopdate || $dbstartdate < $startdate && $dbstopdate > $startdate || $dbstartdate < $stopdate && $dbstopdate > $stopdate) {
                    $this->form_validation->set_message('compare_dates', 'The period time intersects with another one');
                    return false;
                }
            }
        }
        if ($startdate >= $stopdate) {
            $this->form_validation->set_message('compare_dates', 'Starttime is later than stoptime.');
            return false;
        }
        return true;
    }
    
    /**
     * the controller delete method, deletes a period
     * @param type $id
     * @return void
     */
    public function delete($id = 0) {
        
        if (isset($_POST['btnAction']) AND is_array($_POST['action_to'])) {
            $this->periods_m->delete_many($this->input->post('action_to'));
        } elseif (is_numeric($id)) {
            $this->periods_m->delete($id);
        }
        redirect('admin/periods');
    }
}
