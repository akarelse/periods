<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a periods module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website
 * @package     PyroCMS
 * @subpackage  Periods Module
 */
class Periods extends Public_Controller
{
    
    /**
     * Constructor
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('periods_m');
        $this->lang->load('periods');
        
        $this->item_validation_rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|max_length[100]|required'
            ) ,
            array(
                'field' => 'slug',
                'label' => 'Slug',
                'rules' => 'trim|max_length[100]|required'
            ) ,
            array(
                'field' => 'logtime',
                'label' => 'Logtime',
                'rules' => 'required'
            ) ,
            array(
                'field' => 'data',
                'label' => 'Data',
                'rules' => 'trim|max_length[200]'
            ) ,
            array(
                'field' => 'newdata',
                'label' => 'newdata',
                'rules' => 'trim'
            )
        );
    }
    
    /**
     * Index function of constructor for the periods module, in this case only json output
     * @return void
     */
    public function index() {
        $items = Events::trigger('current_period', array() , 'array');
        echo json_encode($items);
    }
    
    /**
     * The function to log numeric data and to return json data in the form of commands or status
     * @return void
     */
    public function logit() {
        $datetime = new DateTime();
        $input = $this->input->post();
        
        $input['logtime'] = $datetime->format('Y-m-d H:i:s');
        $this->form_validation->set_rules($this->item_validation_rules);
        $names = Events::trigger('get_all_names', $input, 'array');
        $names[0]['newdata'] = "newdata";
        $names[0]['dontlog'] = "dontlog";
        $inputa = array_diff_key($input, $names[0]);
        
        $period = Events::trigger('current_period', array() , 'array');
        $calicmds = Events::trigger('calibrate_commands', array() , 'array');
        $items = array_merge($period, $calicmds);
        
        if (isset($input['newdata']) && count($items) > 0) {
            echo json_encode($items);
        } else {
            print ("OK\n");
        }
        
        $this->form_validation->set_data($input);
        
        if (!isset($input['dontlog']) || isset($calicmds['calibrate'])) {
            if ($this->form_validation->run()) {
                $input['data'] = json_encode(str_replace("'", "\"", $inputa));
                unset($input['id']);
                Events::trigger('create_real_graph', $input, 'array');
            } else {
                echo "validation errors\n";
                print_r($input);
            }
        } else {
            if (isset($calicmds['calibrate'])) {
                Events::trigger('calibrate_data_log', $input, 'array');
            } else {
                echo "no logging done\n";
            }
        }
    }
}
