<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Periods Module
 */
class Admin_actuators extends Admin_Controller
{
    protected $section = 'actuators';
    
    // protected $section = 'actuators';
    
    
    
    /**
     * Constructor
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('period_actuators_m');
        
        $this->load->library('form_validation');
        $this->load->library('firephp');
        
        $this->lang->load('periods');
        $this->item_validation_rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|max_length[100]|required'
            ) ,
            array(
                'field' => 'slug',
                'label' => 'Slug',
                'rules' => 'trim|max_length[100]|required'
            ) ,
            array(
                'field' => 'type_id',
                'label' => 'Type',
                'rules' => 'trim|integer'
            )
        );
        
        $this->types = array(
            "Unknown",
            "pomp",
            "lamp",
            "klep",
            "verwarming",
            "ventilator"
        );
        $this->template->append_js('module::admin.js')->append_css('module::admin.css')->append_css('module::jquery-ui-1.10.4.custom.min.css')->append_metadata($this->load->view('fragments/wysiwyg'));
    }
    
    /**
     * List all items
     */
    public function index() {
        $items = $this->period_actuators_m->get_all();
        
        if (count($items) > 0) {
            foreach ($items as $value) {
                $value->type = $this->types[$value->type_id];
                $tempar[] = $value;
            }
            $items = $tempar;
        }
        
        $this->template->title($this->module_details['name'])->set('items', $items)->build('admin/actuators/index');
    }
    
    /**
     * the admin controller for periods create funtion
     * @return void
     */
    public function create() {
        if ($input = $this->input->post()) {
            $data = $this->setrightpost($input);
            $this->form_validation->set_rules($this->item_validation_rules);
            
            if ($this->form_validation->run()) {
                if ($this->period_actuators_m->create($data)) {
                    $this->session->set_flashdata('success', lang('periods.success'));
                    redirect('admin/periods/actuators');
                } else {
                    $this->session->set_flashdata('error', lang('periods.error'));
                    redirect('admin/periods/actuators/create');
                }
            }
        } else {
            foreach ($this->item_validation_rules as $rule) {
                $data[$rule['field']] = $input[$rule['field']];
            }
        }
        
        $data['types'] = $this->types;
        $this->template->title($this->module_details['name'], lang('periods.new_item'))->build('admin/actuators/form', $data);
    }
    
    /**
     * the admin controller for periods edit funtion
     * @return void
     */
    public function edit($id = 0) {
        $data = $this->period_actuators_m->get($id);
        $data->types = $this->types;
        
        if ($input = $this->input->post()) {
            $this->form_validation->set_rules($this->item_validation_rules);
            unset($_POST['btnAction']);
            $data = $this->setrightpost($this->input->post());
            
            if ($this->period_actuators_m->edit($id, $data) && $this->form_validation->run()) {
                $this->session->set_flashdata('success', "OK !");
                redirect('admin/periods/actuators');
            } else {
                $this->session->set_flashdata('error', lang('periods.error'));
                redirect('admin/periods/actuators/create');
            }
        }
        
        $this->pyrocache->delete('period_actuators_m');
        
        $this->template->title($this->module_details['name'], lang('periods.edit'))->build('admin/actuators/form', $data);
    }
    
    /**
     * remove date stuff after it is integrated into something else
     * @param  array $post post data in
     * @return array $post cleaned data out
     */
    private function setrightpost($post) {
        unset($post['start_on']);
        unset($post['start_on_hour']);
        unset($post['start_on_minute']);
        unset($post['btnAction']);
        return $post;
    }
    
    /**
     * the controller delete method, deletes a period
     * @param type $id
     * @return void
     */
    public function delete($id = 0) {
        
        if (isset($_POST['btnAction']) AND is_array($_POST['action_to'])) {
            $this->period_actuators_m->delete_many($this->input->post('action_to'));
        } elseif (is_numeric($id)) {
            $this->period_actuators_m->delete($id);
        }
        redirect('admin/periods/actuators');
    }
}
