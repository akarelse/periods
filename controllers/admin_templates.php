<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Periods Module
 */
class Admin_templates extends Admin_Controller
{
    protected $section = 'templates';
    
    public function __construct() {
        parent::__construct();
        $this->load->model('period_templates_m');
        $this->load->library('form_validation');
        $this->lang->load('periods');
        
        $this->validation_rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|max_length[100]|required'
            ) ,
            array(
                'field' => 'slug',
                'label' => 'Slug',
                'rules' => 'trim|max_length[100]|required'
            ) ,
            array(
                'field' => 'temphigh',
                'label' => 'Temphigh',
                'rules' => 'trim|integer'
            ) ,
            array(
                'field' => 'templow',
                'label' => 'Templow',
                'rules' => 'trim|integer'
            ) ,
            array(
                'field' => 'phhigh',
                'label' => 'Phhigh',
                'rules' => 'trim|integer'
            ) ,
            array(
                'field' => 'phlow',
                'label' => 'Phlow',
                'rules' => 'trim|integer'
            ) ,
            array(
                'field' => 'echigh',
                'label' => 'Echigh',
                'rules' => 'trim|integer'
            ) ,
            array(
                'field' => 'eclow',
                'label' => 'Eclow',
                'rules' => 'trim|integer'
            ) ,
            array(
                'field' => 'waterhigh',
                'label' => 'Waterhigh',
                'rules' => 'trim|integer'
            ) ,
            array(
                'field' => 'waterlow',
                'label' => 'Waterlow',
                'rules' => 'trim|integer'
            ) ,
            array(
                'field' => 'moisthigh',
                'label' => 'Moisthigh',
                'rules' => 'trim|integer'
            ) ,
            array(
                'field' => 'moistlow',
                'label' => 'Moistlow',
                'rules' => 'trim|integer'
            ) ,
            array(
                'field' => 'co2high',
                'label' => 'Co2high',
                'rules' => 'trim|integer'
            ) ,
            array(
                'field' => 'co2low',
                'label' => 'Co2low',
                'rules' => 'trim|integer'
            ) ,
        );
        
        $this->template->append_js('module::admin.js')->append_css('module::admin.css')->append_css('module::jquery-ui-1.10.4.custom.min.css')->append_metadata($this->load->view('fragments/wysiwyg'));
    }
    
    /**
     * of the controller admin templates the index function to show all the templates
     * @return void
     */
    public function index() {
        $data->items = $this->period_templates_m->get_all();
        $this->template->title($this->module_details['name'], lang('periods:iets'))->build('admin/templates/index', $data);
    }
    
    /**
     * the past that creates an template
     * @return void
     */
    public function create() {
        if ($data = $this->input->post()) {
            $this->form_validation->set_rules($this->validation_rules);
            
            if ($id = $this->period_templates_m->create($data) && $this->form_validation->run()) {
                $this->session->set_flashdata('success', lang('periods.success'));
                redirect('admin/periods/templates');
            } else {
                $this->session->set_flashdata('error', lang('periods.error'));
                redirect('admin/periods/templates/create');
            }
        } else {
            foreach ($this->validation_rules AS $rule) {
                if (strpos($rule['rules'], "integer")) {
                    $data->{$rule['field']} = 0;
                } else {
                    $data->{$rule['field']} = "";
                }
            }
        }
        
        $this->pyrocache->delete('period_templates_m');
        
        $this->template->title($this->module_details['name'], lang('periods.new_event'))->build('admin/templates/form', $data);
    }
    
    /**
     * the controller edit method, edit a template
     * @param type $id
     * @return void
     */
    public function edit($id = 0) {
        $data = $this->period_templates_m->get($id);
        if ($input = $this->input->post()) {
            $this->form_validation->set_rules($this->validation_rules);
            unset($_POST['btnAction']);
            
            if ($this->period_templates_m->edit($id, $input) && $this->form_validation->run()) {
                $this->session->set_flashdata('success', lang('periods.success'));
                redirect('admin/periods/templates');
            } else {
                $this->session->set_flashdata('error', lang('periods.error'));
                redirect('admin/periods/templates/edit');
            }
        }
        
        $this->pyrocache->delete('period_templates_m');
        $this->template->title($this->module_details['name'], lang('periods.edit'))->build('admin/templates/form', $data);
    }
    
    /**
     * the controller delete an template function
     * @param type $id
     * @return void
     */
    public function delete($id = 0) {
        
        if (isset($_POST['btnAction']) AND is_array($_POST['action_to'])) {
            $this->period_templates_m->delete_many($this->input->post('action_to'));
        } elseif (is_numeric($id)) {
            $this->period_templates_m->delete($id);
        }
        redirect('admin/periods/templates');
    }
}
