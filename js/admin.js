jQuery(function($) {
    // generate a slug when the user types a title in
    pyro.generate_slug('input[name="name"]', 'input[name="slug"]');
});
$(function() {
    $("#tempslider").slider({
        range: true,
        min: 0,
        max: 50,
        step: 1,
        values: [$("#temphigh").val(), $("#templow").val()],
        slide: function(event, ui) {
            $("#temphigh").val(ui.values[1]);
            $("#templow").val(ui.values[0]);
        }
    });
    $("#phslider").slider({
        range: true,
        min: 0,
        max: 14,
        step: 0.1,
        values: [$("#phhigh").val(), $("#phlow").val()],
        slide: function(event, ui) {
            $("#phhigh").val(ui.values[1]);
            $("#phlow").val(ui.values[0]);
        }
    });
    $("#ecslider").slider({
        range: true,
        min: 0,
        max: 10,
        step: 1,
        values: [$("#echigh").val(), $("#eclow").val()],
        slide: function(event, ui) {
            $("#echigh").val(ui.values[1]);
            $("#eclow").val(ui.values[0]);
        }
    });
    $("#waterslider").slider({
        range: true,
        min: 0,
        max: 4,
        step: 1,
        values: [$("#waterhigh").val(), $("#waterlow").val()],
        slide: function(event, ui) {
            $("#waterhigh").val(ui.values[1]);
            $("#waterlow").val(ui.values[0]);
        }
    });
    $("#moistslider").slider({
        range: true,
        min: 0,
        max: 100,
        step: 1,
        values: [$("#moisthigh").val(), $("#moistlow").val()],
        slide: function(event, ui) {
            $("#moisthigh").val(ui.values[1]);
            $("#moistlow").val(ui.values[0]);
        }
    });
    $("#co2slider").slider({
        range: true,
        min: 0,
        max: 100,
        step: 1,
        values: [$("#co2high").val(), $("#co2low").val()],
        slide: function(event, ui) {
            $("#co2high").val(ui.values[1]);
            $("#co2low").val(ui.values[0]);
        }
    });
    $("#datepicker2").datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $("#datepicker").datepicker({
        dateFormat: 'yy-mm-dd'
    });
});