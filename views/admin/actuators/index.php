<section class="title">
	<h4><?php echo lang('periods:actuator_list'); ?></h4>
</section>
<section class="item">
	<?php if (!empty($items)): ?>
	<?php echo form_open('admin/periods/actuators/delete'); ?>
	<table>
		<thead>
			<tr>
				<th><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
				<th><?php echo lang('periods:name'); ?></th>
				<th><?php echo lang('periods:type'); ?></th>
				<th></th>
			</tr>
		</thead>
		<tfoot>
		<tr>
			<td colspan="5">
				<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
			</td>
		</tr>
		</tfoot>
		<tbody>
			
			<?php foreach( $items as $item ): ?>
			<tr>
				<td><?php echo form_checkbox('action_to[]', $item->id); ?></td>
				<td><?php echo $item->name; ?></td>
				<td><?php echo $item->type; ?></td>
				<td class="actions">
					<?php echo
					anchor('admin/periods/actuators/edit/'.$item->id, lang('periods:edit'), 'class="button"').' '.
					anchor('admin/periods/actuators/delete/'.$item->id, 	lang('periods:delete'), array('class'=>'button')); ?>
				</td>
			</tr>
			<?php endforeach; ?>
			
		</tbody>
	</table>
	<div class="table_action_buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
	</div>
	<?php else: ?>
	<div class="no_data"><?php echo lang('periods:no_items'); ?></div>
	<?php endif;?>
	<?php echo form_close(); ?>
</section>