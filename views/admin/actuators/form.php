<section class="title">
	<h4>
	<?php echo lang('periods:'.$this->method); ?>
	</h4>
</section>
<section class="item">
	<div class="content">
		<?php echo form_open(); ?>
		<div class="tabs">
			<div class="form_inputs" id="page-layout-html">
				<fieldset>
					<ul>
						<li class="even">
							<label for="title"><?php echo lang('global:title');?> <span>*</span></label>
							<div class="input">
								<?php echo form_input('name', $name, 'maxlength="60"'); ?>
							</div>
						</li>
						<li class="even">
							<label for="slug"><?php echo lang('global:slug');?> <span>*</span></label>
							<div class="input">
								<?php echo form_input('slug', $slug, 'maxlength="60"'); ?>
							</div>
						</li>
						<li class="<?php echo alternator('', 'even'); ?>">
							<label><?php echo lang('periods:types'); ?></label>
							<div class="input">
								<?php echo form_dropdown('type_id', $types, $type_id) ?>
							</div>
						</li>
					</ul>
				</fieldset>
			</div>
		</div>
		<div class="buttons float-right padding-top">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel') )); ?>
		</div><?php echo form_close(); ?>
	</div>
</section>