<section class="title">
	<h4><?php echo lang('periods:item_list'); ?></h4>
</section>
<section class="item">
	<?php echo form_open('admin/periods/delete');?>
	<?php if (!empty($items)): ?>
	<table>
		<thead>
			<tr>
				<th><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
				<th><?php echo lang('periods:name'); ?></th>
				<th><?php echo lang('periods:starttime'); ?></th>
				<th><?php echo lang('periods:duration'); ?></th>
				<th></th>
			</tr>
		</thead>
		<tfoot>
		<tr>
			<td colspan="5">
				<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
			</td>
		</tr>
		</tfoot>
		<tbody>
			<?php foreach( $items as $item ): ?>
			<tr>
				<td><?php echo form_checkbox('action_to[]', $item->id); ?></td>
				<td><?php echo $item->name; ?></td>
				<td><?php echo $item->starttime; ?></td>
				<td><?php echo $item->duration; ?></td>
				<td class="actions">
					<?php echo
					anchor('admin/periods/edit/'.$item->id, lang('periods:edit'), 'class="button"').' '.
					anchor('admin/periods/delete/'.$item->id, 	lang('periods:delete'), array('class'=>'button')); ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<div class="table_action_buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
	</div>
	<?php else: ?>
	<div class="no_data"><?php echo lang('periods:no_items'); ?></div>
	<?php endif;?>
	<?php echo form_close(); ?>
</section>