<section class="title">
	<h4>
	<?php echo lang('periods:'.$this->method); ?>
	</h4>
</section>
<section class="item">
	<div class="content">
		<?php echo form_open(); ?>
		<div class="tabs">
			<div class="form_inputs" id="page-layout-html">
				<fieldset>
					<ul>
						<li class="even">
							<label for="title"><?php echo lang('global:title');?> <span>*</span></label>
							<div class="input">
								<?php echo form_input('name', $name, 'maxlength="60"'); ?>
							</div>
						</li>
						<li class="even">
							<label for="slug"><?php echo lang('global:slug');?> <span>*</span></label>
							<div class="slug">
								<?php echo form_input('slug', $slug, 'maxlength="60"'); ?>
							</div>
						</li>
						<div class="input" id="tempslider">
						</div>
						<li class="even">
							<label for="temp"><?php echo lang('periods:temp');?> <span>*</span></label>
							<?php echo form_input(array('class'=>'disabled','readonly'=> 'readonly', 'id' => 'templow','name' => 'templow', 'value' => $templow, 'maxlength'=>'30')); ?>
							<?php echo form_input(array('class'=>'disabled','readonly'=> 'readonly', 'id' => 'temphigh','name' => 'temphigh', 'value' => $temphigh, 'maxlength'=>'30')); ?>
						</li>
						<div class="input" id="phslider">
						</div>
						<li class="even">
							<label for="ph"><?php echo lang('periods:ph');?> <span>*</span></label>
							<?php echo form_input(array('class'=>'disabled','readonly'=> 'readonly', 'id' => 'phlow','name' => 'phlow', 'value' => $phlow, 'maxlength'=>'30')); ?>
							<?php echo form_input(array('class'=>'disabled','readonly'=> 'readonly', 'id' => 'phhigh','name' => 'phhigh', 'value' => $phhigh, 'maxlength'=>'30')); ?>
						</li>
						<div class="input" id="ecslider">
						</div>
						<li class="even">
							<label for="ec"><?php echo lang('periods:ec');?> <span>*</span></label>
							<?php echo form_input(array('class'=>'disabled','readonly'=> 'readonly', 'id' => 'eclow','name' => 'eclow', 'value' => $eclow, 'maxlength'=>'30')); ?>
							<?php echo form_input(array('class'=>'disabled','readonly'=> 'readonly', 'id' => 'echigh','name' => 'echigh', 'value' => $echigh, 'maxlength'=>'30')); ?>
						</li>
						<div class="input" id="waterslider">
						</div>
						<li class="even">
							<label for="water"><?php echo lang('periods:water');?> <span>*</span></label>
							<?php echo form_input(array('class'=>'disabled','readonly'=> 'readonly', 'id' => 'waterlow','name' => 'waterlow', 'value' => $waterlow, 'maxlength'=>'30')); ?>
							<?php echo form_input(array('class'=>'disabled','readonly'=> 'readonly', 'id' => 'waterhigh','name' => 'waterhigh', 'value' => $waterhigh, 'maxlength'=>'30')); ?>
						</li>
						<div class="input" id="moistslider">
						</div>
						<li class="even">
							<label for="moist"><?php echo lang('periods:moist');?> <span>*</span></label>
							<?php echo form_input(array('class'=>'disabled','readonly'=> 'readonly', 'id' => 'moistlow','name' => 'moistlow', 'value' => $moistlow, 'maxlength'=>'30')); ?>
							<?php echo form_input(array('class'=>'disabled','readonly'=> 'readonly', 'id' => 'moisthigh','name' => 'moisthigh', 'value' => $moisthigh, 'maxlength'=>'30')); ?>
						</li>
						<div class="input" id="co2slider">
						</div>
						<li class="even">
							<label for="co2"><?php echo lang('periods:co2');?> <span>*</span></label>
							<?php echo form_input(array('class'=>'disabled','readonly'=> 'readonly', 'id' => 'co2low','name' => 'co2low', 'value' => $co2low, 'maxlength'=>'30')); ?>
							<?php echo form_input(array('class'=>'disabled','readonly'=> 'readonly', 'id' => 'co2high','name' => 'co2high', 'value' => $co2high, 'maxlength'=>'30')); ?>
						</li>
					</ul>
				</fieldset>
			</div>
		</div>
		<div class="buttons float-right padding-top">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel') )); ?>
		</div><?php echo form_close(); ?>
	</div>
</section>