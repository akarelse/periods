<section class="title">
	<h4><?php echo lang('periods:'.$this->method); ?></h4>
</section>
<section class="item">
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
	<div class="form_inputs">
		<ul>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="name"><?php echo lang('global:title'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('name', set_value('name', $name), 'class="width-15"'); ?></div>
			</li>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="slug"><?php echo lang('global:slug'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('slug', set_value('slug', $slug), 'class="width-15"'); ?></div>
			</li>
			<li class="date-meta">
				<label><?php echo lang('periods:starttime'); ?><span>*</span></label>
				<div class="input datetime_input">
					<?php echo form_input('start_on', date('Y-m-d', $rightstarttime), 'maxlength="10" id="datepicker" class="text width-20"'); ?>&nbsp;
				</div>
				<label><?php echo lang('periods:hours'); ?><span>*</span></label>
				<div class="input datetime_input">
					<?php echo form_dropdown('start_on_hour', $hours, date('H', $rightstarttime)) ?>
				</div>
				<label><?php echo lang('periods:minutes'); ?><span>*</span></label>
				<div class="input datetime_input">
					<?php echo form_dropdown('start_on_minute', $minutes, date('i', $rightstarttime)); ?>
				</div>
			</li>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="duration"><?php echo lang('global:duration'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('duration', set_value('duration', $duration), 'class="width-15"'); ?></div>
			</li>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label><?php echo lang('periods:template'); ?></label>
				<div class="input">
					<?php echo form_dropdown('template_id', $templates, $template_id) ?>
				</div>
			</li>
		</ul>
	</div>
	<div class="buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
	</div>
	<?php echo form_close(); ?>
</section>