<?php
//messages
$lang['periods:success']			=	'It worked';
$lang['periods:error']			=	'It didn\'t work';
$lang['periods:no_items']		=	'No Items';

//page titles
$lang['periods:create']			=	'Create Item';

//labels
$lang['periods:name']			=	'Name';
$lang['periods:slug']			=	'Slug';
$lang['periods:manage']			=	'Manage';
$lang['periods:item_list']		=	'Item List';
$lang['periods:template_list']	=	'Template List';
$lang['periods:actuator_list']	=	'Actuator List';
$lang['periods:view']			=	'View';
$lang['periods:edit']			=	'Edit';
$lang['periods:delete']			=	'Delete';
$lang['periods:temp']			=	'Temperature Max - Min';
$lang['periods:ph']				=	'Ph Max - Min';
$lang['periods:ec']				=	'Ec Max - Min';
$lang['periods:water']			=	'Water level Max - Min';
$lang['periods:moist']			=	'Air moisture Max - Min';
$lang['periods:co2']			=	'Co2 level Max -Min';
$lang['periods:templates']		=	'Templates';
$lang['periods:starttime']		=	'Start at';
$lang['periods:stoptime']		=	'Stop at';
$lang['periods:template']		=	'Template';
$lang['periods:hours']			=	'hours';
$lang['periods:minutes']		=	'minutes';
$lang['periods:types'] 			=	'types';


//buttons
$lang['periods:custom_button']	=	'Custom Button';
$lang['periods:periods']			=	'Periods';
$lang['periods:actuators']			=	'Actuators';
?>