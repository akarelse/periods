<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Module_Periods extends Module
{
    
    public $version = '1.0';
    const MIN_PHP_VERSION = '5.2.0';
    const MIN_PYROCMS_VERSION = '2.1';
    const MAX_PYROCMS_VERSION = '2.2.3';
    
    public function info() {
        return array(
            'name' => array(
                'en' => 'Periods'
            ) ,
            'description' => array(
                'en' => 'This is the growth panel periods module.'
            ) ,
            'frontend' => FALSE,
            'backend' => TRUE,
            'menu' => 'content',
            'sections' => array(
                'periods' => array(
                    'name' => 'periods:periods',
                    'uri' => 'admin/periods',
                    'shortcuts' => array(
                        'create' => array(
                            'name' => 'periods:create',
                            'uri' => 'admin/periods/create',
                            'class' => 'add'
                        )
                    )
                ) ,
                'templates' => array(
                    'name' => 'periods:templates',
                    'uri' => 'admin/periods/templates',
                    'shortcuts' => array(
                        'create' => array(
                            'name' => 'periods:create_template',
                            'uri' => 'admin/periods/templates/create',
                            'class' => 'add'
                        )
                    )
                ) ,
                'actuators' => array(
                    'name' => 'periods:actuators',
                    'uri' => 'admin/periods/actuators',
                    'shortcuts' => array(
                        'create' => array(
                            'name' => 'periods:create_template',
                            'uri' => 'admin/periods/actuators/create',
                            'class' => 'add'
                        )
                    )
                ),
                'sensors' => array(
                    'name' => 'periods:sensors',
                    'uri' => 'admin/periods/sensors',
                    'shortcuts' => array(
                        'create' => array(
                            'name' => 'periods:create_template',
                            'uri' => 'admin/periods/sensors/create',
                            'class' => 'add'
                        )
                    )
                )
            )
        );
    }
    
    public function install() {
        
        if (!$this->check_php_version()) {
            $this->session->set_flashdata('error', 'Need a higher php version !');
            return FALSE;
        }
        
        if (!$this->check_pyrocms_version()) {
            $this->session->set_flashdata('error', 'Not the right PyroCMS version !');
            return FALSE;
        }
        
        $periods = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ) ,
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ) ,
            'slug' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ) ,
            'starttime' => array(
                'type' => 'DATETIME',
                'null' => false
            ) ,
            'duration' => array(
                'type' => 'INT',
                'null' => false
            ) ,
            'template_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => '0'
            )
        );
        
        $templates = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ) ,
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ) ,
            'slug' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ) ,
            'temphigh' => array(
                'type' => 'INT',
                'constraint' => '10'
            ) ,
            'templow' => array(
                'type' => 'INT',
                'constraint' => '10'
            ) ,
            'phhigh' => array(
                'type' => 'INT',
                'constraint' => '10'
            ) ,
            'phlow' => array(
                'type' => 'INT',
                'constraint' => '10'
            ) ,
            'echigh' => array(
                'type' => 'INT',
                'constraint' => '10'
            ) ,
            'eclow' => array(
                'type' => 'INT',
                'constraint' => '10'
            ) ,
            'waterhigh' => array(
                'type' => 'INT',
                'constraint' => '10'
            ) ,
            'waterlow' => array(
                'type' => 'INT',
                'constraint' => '10'
            ) ,
            'moisthigh' => array(
                'type' => 'INT',
                'constraint' => '10'
            ) ,
            'moistlow' => array(
                'type' => 'INT',
                'constraint' => '10'
            ) ,
            'co2high' => array(
                'type' => 'INT',
                'constraint' => '10'
            ) ,
            'co2low' => array(
                'type' => 'INT',
                'constraint' => '10'
            )
        );
        $actuators = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ) ,
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ) ,
            'slug' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ) ,
            'type_id' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => '0'
            )
        );

        $sensors = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ) ,
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ) ,
            'slug' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ) ,
            'type_id' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => '0'
            )
        );
        
        $this->db->trans_start();
        
        $this->dbforge->add_field($periods);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('periods');
        
        $this->dbforge->add_field($actuators);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('period_actuators');

        $this->dbforge->add_field($sensors);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('sensors');
        
        $this->dbforge->add_field($templates);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('period_templates');
        
        $this->db->trans_complete();
        
        return $this->db->trans_status();
    }
    
    public function uninstall() {
        if ($this->dbforge->drop_table('periods') && $this->dbforge->drop_table('period_templates')) {
            return TRUE;
        }
        return FALSE;
    }
    
    public function upgrade($old_version) {
        if (!$this->check_php_version()) {
            $this->session->set_flashdata('error', 'Need a higher php version !');
            return FALSE;
        }
        if (!$this->check_pyrocms_version()) {
            $this->session->set_flashdata('error', 'Not the right PyroCMS version !');
            return FALSE;
        }
        return TRUE;
    }
    
    public function help() {
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }
    
    /**
     * Check the current version of PHP and thow an error if it's not good enough
     *
     * @access private
     * @return boolean
     * @author Victor Michnowicz
     */
    private function check_php_version() {
        if (version_compare(PHP_VERSION, self::MIN_PHP_VERSION) < 0) {
            show_error('This addon requires PHP version ' . self::MIN_PHP_VERSION . ' or higher.');
            return FALSE;
        }
        return TRUE;
    }
    
    private function check_pyrocms_version() {
        if (version_compare(CMS_VERSION, self::MIN_PYROCMS_VERSION) < 0 && version_compare(CMS_VERSION, self::MAX_PYROCMS_VERSION) > 0) {
            show_error('This addon requires PYROCMS version ' . self::MIN__VERSION . ' minnimal or version ' . MAX_PYROCMS_VERSION . 'maximal.');
            return FALSE;
        }
        return TRUE;
    }
}
